import 'package:flutter/material.dart';

const primaryColor = Color.fromARGB(255, 230, 223, 22);
const secondaryColor = Color.fromARGB(240, 12, 5, 3);


const defaultPadding = EdgeInsets.symmetric(horizontal: 30);

TextStyle titleText =
    const TextStyle(color: primaryColor, fontSize: 32, fontWeight: FontWeight.w700);
TextStyle subTitle = const TextStyle(
    color: secondaryColor, fontSize: 18, fontWeight: FontWeight.w500);
TextStyle textButton = const TextStyle(
  color: primaryColor,
  fontSize: 18,
  fontWeight: FontWeight.w700,
);