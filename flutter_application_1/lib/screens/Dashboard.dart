import 'package:flutter/material.dart';
import 'package:flutter_application_1/widgets/MyDashboard.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: const BoxDecoration(
        image: DecorationImage(
            image: AssetImage("lib/assets/Forest.jpg"), fit: BoxFit.cover),
      )),
      drawer: const NavDrawer(),
      appBar: AppBar(title: const Text('Menu'),
      ),
      
    );
  }
}
